<section class="banner">
        <div class="welcome-text">
            <div class="item">
                <h1>
                    Let us help your <span style="color: #0091ff">Business</span>
                    <br>
                    with <span style="color: #0091ff">Financing</span>
                </h1>
                <p>Starting an SME from scratch is hard work.
                    <br>
                    Achieve new heights for your business with our business loan.</p>
                <button>
                    Expand Today
                </button>
            </div>
        </div>
        <div class="img-container">
            <div class="img-wrapper">
                <img src="/img/main-img-1.png" alt="" id="banner-image-one" />
                <img src="/img/img-2.png" alt="" id="banner-image-two" />
                <img src="/img/img-3.png" alt="" id="banner-image-three" />
            </div>
        </div>
    </section>